// console.log("hello batch 243");

// [Section] Function
	// Function
		// Functions in JS are lines/blocks of codes taht tell our device/application to perform a certain task when called or invoke.
		// Functions are mostly created to create a complicated task to run several lines of code in succession.
		// They are also used to prevent repeating lines/blocks of codes that perform the same task or function.

	// Function Declaration
		// (function statements) - defines a function with specified parameters.

		/*Syntax:
			function functionName(){
				code block (statements)
			}
		*/

		// function keyword - used to define a javascript functions.
		// functionName - the function name. Functions are named to be able to use later in the code.
		// function block ({}) - the statements which comprise the body of the function. This is where the code will be executed.

			function printName(){
				console.log("My name is John.")
				console.log("My last name is Dela Cruz.")
			}

	// Function Invocation:
		// The code block and statements inside the function is not immediately executed when the function is defined/declared. The code block and statements inside a function is executed when the function is invoked.
		// It is common to use the term "Call a function" instead of "Invoke a function"

			// Let's invoke a function that we declared.
			printName();
			// Functions are reusable.
			printName();

// [Section] Function Declaration and Function Expression
	// Function Declaration
		// Function can be created through function declaration by using the keyword function and adding the function name.
		// Declared Functions are executed immediately.

			declaredFunction();

			function declaredFunction(){
				console.log("Hello World from declaredFunction")
			}

	// Function expression
		// A function can also be stored in a variable. This is called the function expression.

		// Anonymous function - functions without a name.

			// variableFunction();
		let variableFunction = function(){
			console.log("Hello for variableFunction!");
		}

		variableFunction();

		let funcExpression = function funcName(){
			console.log("Hello from funcExpression!");
		}

		funcExpression();

	// you can reassign declared function and function expression to new anonymous function.

		declaredFunction = function(){
			console.log("Updated declaredFunction");
		}

		declaredFunction();

		funcExpression = function(){
			console.log("Updated funcExpression")
		}

		funcExpression();

		// Function expression using const keyword
		const constantFunc = function(){
			console.log("Initialized with const!")
		}

		constantFunc();

		/*constantFunc = function(){
			console.log("Cannot be reassigned");
		}

		constantFunc();*/

// [Section] Function Scoping

/*
	Scope is accessibility of variable within our program.

	JavaScript variables has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope
*/

		{
			let localVar = "Armando Perez";
			console.log(localVar);
		}

	let globalVar = "Mr. Worldwide";

	// function scope
	// JS has function scope: Each function creates a new scope.
	// Variables defined inside a function are not accessible outside the function.
	
	/*function showNames(){
		let functionLet = "Jane";
		const functionConst = "John";

		console.log(functionConst);
		console.log(functionLet);

	}

	showNames();*/

	//The variables, functionLet and functionConst, are functions scoped and cannot be accessed outside of the function that they were declared.
	/*console.log(functionConst);
	console.log(functionLet);*/

// [Section] Nested Functions
	// You can create another function inside a function. This is called nested function.

	function myNewFunction(){
		let name = "Jane";
		console.log(name);
		function nestedFunction(){
			let nestedName = "John";

			console.log(nestedName);
			console.log(name);
		}
		nestedFunction();
	}

	myNewFunction();

	// Function and Global Scoped Variables
		// Global Scoped Variable
		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz";

			console.log(globalName);
			console.log(nameInside);
		}

		myNewFunction2();


// [Section] Using Alert
		// alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to console.log() which only shows on the console. It allows us to show a short dialog or instruction to our users. The page will wait until the user dismiss the dialog.

		alert("Hello World"); //This will run immediately when the page loads

		// Syntax:
		// alert("<messageInString>");

		function showSampleAlert(){
			alert("Hello, User!");
		}

		showSampleAlert();

		// Notes on use of alert()
			// Show only an alert() for short dialogs/messages to the user.
			// Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// [Section] prompt()

		// prompt allowsus to show a small window at the top of the browser to gather user input. The input from the prompt() wil be returned as a string once the user dismisses the window.
		let samplePrompt = prompt("Enter your name:");
			console.log(samplePrompt);
			console.log(typeof samplePrompt);

			/*
				Syntax:
					prompt("<dialogInString>")
			*/

		let sampleNullPrompt = prompt("Don't enter anything:");
		console.log(sampleNullPrompt);
		console.log(typeof sampleNullPrompt);

		function printWelcomeMessages(){
			let firstName = prompt("Enter your first name: ");
			let lastName = prompt("Enter your last name: ");
		
			console.log("Hello, " + firstName + " " + lastName + "!");
		}

		printWelcomeMessages();

// [Section] Function Naming Convention
		
		// Function Names should be definitive of the task it will perform. It usually contains a verb.

			function getCourses(){
				let course = ["Science 101", "Math 101", "English 101"];

				console.log(course);
			}

			getCourses();

		// Avoid generic name to avoid confusion within your code/program
			function get(){
				let name ="Jamie";
				console.log(name);
			}

			get();

		// Avoid pointless and inappropriate function names.
			function foo(){
				console.log(25%5);
			}

			foo();

		// Name your function in small caps and follow camelCasing when naming variables and functions.
			function displayCarInfo(){
				console.log("Brand: Toyota");
				console.log("Type: Sedan");
				console.log("Price: 1,500,000");
			}

			displayCarInfo();